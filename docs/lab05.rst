**********************
Лабораторная работа №5
**********************

Цель работы
===========
Изучение системных вызовов для работы с файлами и переменными окружения.

Содержание  работы
==================

1.    Запустить операционную систему.
2.    Войти в виртуальную машину, контейнер или на удалённый сервер приложений (c IP адресом XX.XX.XX.254, пользователь lxc<NN>, пароль спросить у преподавателя).
3.    В домашнем каталоге пользователя создать каталог work.
4.    Написать программу, которая сохраняет значения аргументов командной строки и параметров окружающей среды в файл lab5.txt.
5.    Написать makefile, обеспечивающий трансляцию, установку, очистку и удаление программы (см. лаб. 4)
6.    Оттранслировать программу и установить её в каталог bin каталога work с помощью команды make.
7.    Очистить каталог work от вспомогательных файлов с помощью команды make.
8.    Запустить оттранслированную программу.
9.    Представить результаты выполнения работы преподавателю.

Ход работы
==========
Dockerfile 
----------
::

    FROM centos
    RUN yum -y install bzip2 make gcc
    COPY lab05.c /
    COPY Makefile /
    COPY script.sh /
    CMD sh script.sh

sh-скрипт
---------
::

    echo "1.    Запустить операционную систему." >> lab5.txt
    echo "2.    Войти в виртуальную машину, контейнер или на удалённый сервер приложений (c IP адресом XX.XX.XX.254, пользователь lxc<NN>, пароль спросить у преподавателя)." >> lab5.txt
    echo "3.    В домашнем каталоге пользователя создать каталог work." >> lab5.txt
    echo "$ mkdir work" >> lab5.txt
    mkdir work >> lab5.txt
    echo "4.    Написать программу, которая сохраняет значения аргументов командной строки и параметров окружающей среды в файл lab5.txt." >> lab5.txt
    echo "5.    Написать makefile, обеспечивающий трансляцию, установку, очистку и удаление программы (см. лаб. 4)" >> lab5.txt
    echo "6.    Оттранслировать программу и установить её в каталог bin каталога work с помощью команды make." >> lab5.txt
    echo "$ make install" >> lab5.txt
    make install >> lab5.txt
    echo "7.    Очистить каталог work от вспомогательных файлов с помощью команды make." >> lab5.txt
    echo "$ make clean" >> lab5.txt
    make clean >> lab5.txt
    echo "8.    Запустить оттранслированную программу." >> lab5.txt
    echo "$ ./bin/lab5 -f flagvalue example argument" >> lab5.txt
    ./bin/lab05 -f flagvalue example argument>> lab5.txt
    echo "9.    Представить результаты выполнения работы преподавателю." >> lab5.txt
    cat lab5.txt
    
Исходный код программы
----------
::

    #include <stdio.h>
    #include <stdlib.h>
    
    int main(int argc, char* argv[], char* envp[]) {
    	FILE *fp;
    	int i=0;
    	fp=fopen("lab5.txt","a");
    	printf("logging %d argument(s)",argc);
    	for(i=0;i<argc;i++) {
    		fprintf(fp,"Argument %d : %s\n",i,argv[i]);
    	}
    	for(i=0;envp[i]!=NULL;i++) {
    		fprintf(fp,"Environment:%s\n",envp[i]);
    	}
    	printf("and %d environment variables\n",i);
    	fclose(fp);
    }



Протокол выполнения работы
==================================================
1.    Запустить операционную систему.
2.    Войти в виртуальную машину, контейнер или на удалённый сервер приложений (c IP адресом XX.XX.XX.254, пользователь lxc<NN>, пароль спросить у преподавателя).
3.    В домашнем каталоге пользователя создать каталог work.

::
 
 $ mkdir work
 
4.    Написать программу, которая сохраняет значения аргументов командной строки и параметров окружающей среды в файл lab5.txt.
5.    Написать makefile, обеспечивающий трансляцию, установку, очистку и удаление программы (см. лаб. 4)
6.    Оттранслировать программу и установить её в каталог bin каталога work с помощью команды make.

::

    $ make install
    gcc -o lab05 lab05.c
    cp lab05 bin

7.    Очистить каталог work от вспомогательных файлов с помощью команды make.

::

    $ make clean
    rm lab05
    
8.    Запустить оттранслированную программу.

::

    $ ./bin/lab5 -f flagvalue example argument
    Argument 0 : ./bin/lab05
    Argument 1 : -f
    Argument 2 : flagvalue
    Argument 3 : example
    Argument 4 : argument
    Environment:HOSTNAME=74e074f690e3
    Environment:PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    Environment:PWD=/
    Environment:HOME=/root
    Environment:SHLVL=2
    Environment:_=./bin/lab05
    logging 5 argument(s)and 6 environment variables

9.    Представить результаты выполнения работы преподавателю.
